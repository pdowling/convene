#! /bin/sh

# Constants
readonly SCRIPT=$(cd "$(dirname "$0")"; pwd)
readonly USERNAME=rweindl
readonly MONGODB_USER='admin'
readonly MONGODB_PASS='6F466E5F-D27E-48DC-98C1-A35C01F2E0DA'

# Stop and remove the convene-nodejs and convene-mongodb container.
# The convene-data container is not removed for persistency reasons. For a fresh installation add data to the stop and rm commands.
docker stop convene-nodejs convene-mongodb
docker rm convene-nodejs convene-mongodb

###########################
### The Data container. ###
###########################

# Build the convene-data container.
docker build -t $USERNAME/convene-data $SCRIPT/convene-data

# Run the Data container storing and persisting all data.
docker run -itd --name convene-data $USERNAME/convene-data

##############################
### The MongoDB container. ###
##############################

# Build the convene-mongodb container.
docker build -t $USERNAME/convene-mongodb $SCRIPT/convene-mongodb

# Run the MongoDB container.
docker run -itd -p 49002:27017 --volumes-from convene-data -e MONGODB_USER=$MONGODB_USER -e MONGODB_PASS=$MONGODB_PASS --name convene-mongodb $USERNAME/convene-mongodb

##############################
### The Node.js container. ###
##############################

# Build the convene-nodejs container.
docker build -t $USERNAME/convene-nodejs $SCRIPT/convene-nodejs

# Run the node.js container linked to the MongoDB container.
docker run -itd -p 49000:8080 -p 49001:8443 --link convene-mongodb:convene-mongodb --volumes-from convene-data --name convene-nodejs $USERNAME/convene-nodejs

var bodyParser = require('body-parser')
var express = require('express')
var http = require('http')
var https = require('https')
var fs = require('fs')
var mongoose = require('mongoose');
var async = require('async');

// Models.
var Comment = require('./models/Comment.js')
var Ranking = require('./models/Ranking.js')
var User = require('./models/User.js')
var Note = require('./models/Note.js')

// Database initialization.
var mongo_address = process.env['CONVENE_MONGODB_PORT_27017_TCP_ADDR'] || '127.0.0.1';
var mongo_port = process.env['CONVENE_MONGODB_PORT_27017_TCP_PORT'] || '27001';
// process.env['CONVENE_MONGODB_ENV_MONGODB_USER']
// process.env['CONVENE_MONGODB_ENV_MONGODB_PASS']
mongodbAddress = "mongodb://" + 'convene'+ ":" + 'asdf' + "@" + mongo_address + ":" + mongo_port + "/convene";
mongoose.connect(mongodbAddress);

// Application initialization.
var app = express()
app.use(bodyParser.json())

// Routes.
app.get('/', function(req, res) {
  res.send('convene-node-server is up and running…');
});

// GET a list of comments for a given url as paramter.
app.get('/comments', function(req, res) {
  // Receive the URL parameter.
  var url_param = req.query.url
  Comment.find({url: decodeURIComponent(url_param)}).populate('rankings').exec(function(err, comments) {
    if (err) res.send(err)
    else if (!comments) {
      res.status(404);
    } else {
      var commentAggregation = [];
      async.map(comments, function(comment, cb) {
        comment.score = 0;
        if (comment.rankings) {
          comment.rankings.forEach(function(ranking) {
            if (ranking.type === 'up') {
              comment.score += 1;
            } else if (ranking.type === 'down') {
              comment.score -= 1;
            }
          });
        }
        commentAggregation.push(comment);
        cb(null);
      }, function(err) {
        if (err) res.send(err);
        else {
          res.status(200).send({"comments": commentAggregation});
        }
      });
    }
  });
});

// GET a user for a given id.
app.get('/users/:user_id', function(req, res) {
  User.find({_id: req.params.user_id}).exec(function(err, user) {
    if (err) res.send(err)
    else res.status(200).send({"user": user});
  });
});

// GET the rankings for a comment.
app.get('/comments/:comment_id/rankings', function(req, res) {
  Ranking.find({comment: req.params.comment_id}).exec(function(err, rankings) {
    if (err) res.send(err)
    else res.status(200).send({"rankings": rankings})
  })
});

// GET a note.
app.get('/notes', function(req, res) {
  var url_param = req.query.url
  Note.findOne({url: decodeURIComponent(url_param)}).exec(function (err, note) {
    if (err) res.send(err)
    else res.status(200).send({"note": note})
  });
});

// POST a ranking for a comment.
app.post('/comments/:comment_id/ranking', function(req, res) {
  Comment.findOne({_id: req.params.comment_id}).exec(function(err, comment) {
    if (err) {
      res.send(err);
    } else if (!comment) {
      res.send(404);
    } else {
      var ranking = new Ranking(req.body);
      ranking.comment = comment._id;
      ranking.save(function(err, ranking) {
        if (err) res.send(err);
        else {
          comment.rankings.addToSet(ranking);
          comment.save(function (err, comment) {
            if (err) res.send(err)
            else res.status(201).send({"ranking": ranking});
          });
        }
      });
    }
  });
});

// POST a comment.
app.post('/comment', function(req, res) {
  var comment = new Comment(req.body);
  comment.save(function(err, comment) {
    if (err) res.send(err)
    else res.status(201).send({"comment": comment});
  });
});

// POST a user.
app.post('/user', function(req, res) {
  var user = new User(req.body);
  console.log('POST /user ->' + JSON.stringify(req.body));
  user.save(function(err, user) {
    if (err) res.send(err);
    else res.status(201).send({"user": user});
  });
});

// POST a note.
app.post('/note', function(req, res) {
  if (req.body.url && req.body.content && req.body.user) {
    Note.update({url: req.body.url, user: req.body.user}, {url: req.body.url, user: req.body.user, content: req.body.content}, {upsert: true}, function(err, note) {
      if (err) res.send(err);
      else res.status(201).send({"note": note});
    });
  } else {
    res.status(400);
  }
});

// Constants.
var options = {
  key: fs.readFileSync('/src/ssl/server.key'),
  cert: fs.readFileSync('/src/ssl/server.crt'),
  ca: fs.readFileSync('/src/ssl/ca.crt'),
  requestCert: true,
  rejectUnauthorized: false
};

// Startup.
http = http.createServer(app);
https = https.createServer(options, app)
http.listen(8080, function() {
  console.log('convene http is up and running…');
});
https.listen(8443, function() {
  console.log('convene https is up and running…');
});

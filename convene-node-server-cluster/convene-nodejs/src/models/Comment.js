var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 *  CommentSchema.
 */
var CommentSchema = new Schema({
  /* The parameter _id is given as default by MongoDB. */

  /* Comment's target URL */
  url: {
    type: String,
    trim: true,
    required: true
  },

  /* Comment's content. */
  content: {
    type: String,
    trim: true,
    required: true
  },

  /* Comment's user. */
  user: {
    type: Schema.Types.ObjectId,
    ref: 'User',
  },

  /* Comment's responses. */
  parent: {
    type: Schema.Types.ObjectId,
    ref: 'Comment'
  },

  score: {
    type: Number
  },

  /* Comment's rankings.
   * TODO: On later version don't store rankings in Comment model. Rather great child->parent relationship!
   **/
  rankings: [{
    type: Schema.Types.ObjectId,
    ref: 'Ranking'
  }],

  /* Comment creation timestamp. */
  created_at: {
    type: Date,
    default: Date.now
  }
});

/**
 *  Export the CommentSchema in order to use it as an Comment object.
 */
var Comment = mongoose.model('Comment', CommentSchema);
module.exports = Comment;

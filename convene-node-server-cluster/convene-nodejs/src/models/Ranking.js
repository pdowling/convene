var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var validateRankingType = function(type) {
	return (type === 'up' || type === 'down');
};

/**
 *  RankingSchema.
 */
var RankingSchema = new Schema({
  /* The parameter _id is given as default by MongoDB. */

  /* Ranking's comment. */
  comment: {
    type: Schema.Types.ObjectId,
    ref: 'Comment',
    required: true
  },

  /* Ranking's user. */
  user: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  },

  /* Ranking's type. Allowed values are 'up' and 'down'. */
  type: {
    type: String,
    required: true,
    validate: [validateRankingType, 'Please specify a correct ranking type.']
  },

  /* Ranking's creation date. */
  created_at: {
    type: Date,
    default: Date.now
  }
});

/**
 *  Export the RankingSchema in order to use it as an Ranking object.
 */
var Ranking = mongoose.model('Ranking', RankingSchema);
module.exports = Ranking;

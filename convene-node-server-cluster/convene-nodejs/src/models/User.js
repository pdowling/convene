var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 *  UserSchema.
 */
var UserSchema = new Schema({
  /* The parameter _id is given as default by MongoDB. */

  /* User's first name. */
  fist_name: {
    type: String,
    trim: true
  },

  /* User's last name. */
  last_name: {
    type: String,
    trim: true
  },

  /* User's description. */
  description: {
    type: String,
    trim: true
  },

  /* User's EMail. */
  email: {
    type: String,
    trim: true,
    required: true
  },

  /* User's creation date. */
  creation_date: {
    type: Date,
    default: Date.now
  }
});

/**
 *  Export the UserSchema in order to use it as an User object.
 */
var User = mongoose.model('User', UserSchema);
module.exports = User;

var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 *  NoteSchema.
 */
var NoteSchema = new Schema({
  /* The parameter _id is given as default by MongoDB. */

  /* Note's URL */
  url: {
    type: String,
    trim: true,
    required: true
  },

  /* Note's content. */
  content: {
    type: String,
    trim: true,
    required: true
  },

  /* Note's user. */
  user: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },

  /* Note creation timestamp. */
  created_at: {
    type: Date,
    default: Date.now
  }
});

/**
 *  Export the NoteSchema in order to use it as a Note object.
 */
var Note = mongoose.model('Note', NoteSchema);
module.exports = Note;

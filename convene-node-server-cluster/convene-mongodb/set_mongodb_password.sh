if [ -f /.mongodb_password_set ]; then
	echo "MongoDB password already set!"
	exit 0
fi

/usr/bin/mongod --smallfiles --dbpath=/data/convene-mongodb/db &

PASS=${MONGODB_PASS:-$(pwgen -s 12 1)}
_word=$( [ ${MONGODB_PASS} ] && echo "preset" || echo "random" )

RET=1
while [[ RET -ne 0 ]]; do
    echo "=> Waiting for confirmation of MongoDB service startup"
    sleep 5
    mongo admin --eval "help" >/dev/null 2>&1
    RET=$?
done

echo "=> Creating an admin user with a ${_word} password in MongoDB"
mongo admin --eval "db.createUser({user: 'admin', pwd: '$PASS', roles: [ {'role' : 'root', 'db' : 'admin'}, {'role' : 'clusterAdmin', 'db' : 'admin'}, {'role' : 'userAdminAnyDatabase', 'db' : 'admin'}, { 'role' : 'readAnyDatabase', 'db' : 'admin'} ]});"
mongo convene --eval "db.createUser({user: 'convene', pwd: 'asdf', roles: [ { 'role' : 'dbOwner', 'db' : 'convene'}]});"
mongo admin --eval "db.shutdownServer();"

echo "=> Done!"
touch /.mongodb_password_set

echo "========================================================================"
echo "You can now connect to this MongoDB server using:"
echo ""
echo "    mongo admin -u convene -p $PASS --host <host> --port <port>"
echo ""
echo "Please remember to change the above password as soon as possible!"
echo "========================================================================"

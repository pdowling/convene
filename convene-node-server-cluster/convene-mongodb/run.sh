#!/bin/bash
if [ ! -f /.mongodb_password_set ]; then
	/set_mongodb_password.sh
fi

if [ "$AUTH" == "yes" ]; then
    export mongodb='/usr/bin/mongod --port=27017 --dbpath=/data/convene-mongodb/db --logpath=/data/convene-mongodb/log/db.dat --auth --smallfiles --logappend'
else
    export mongodb='/usr/bin/mongod --port=27017 --dbpath=/data/convene-mongodb/db --logpath=/data/convene-mongodb/log/db.dat --smallfiles --logappend'
fi

if [ ! -f /data/db/mongod.lock ]; then
    eval $mongodb
else
    export mongodb=$mongodb' --dbpath /data/convene-mongodb/db'
    rm /data/convene-mongodb/db/mongod.lock
    mongod --dbpath /data/convene-mongodb/db --repair && eval $mongodb
fi

# README #

### What is this repository for? ###

A chrome extension with NLP backends to create a mixture of personal notebook and research utility over any regular webpage.

### How do I get set up? ###

* Download it
* Go to Chrome -> Extensions -> Enable Developer Mode -> Load unpacked extension -> Choose chrome-overlay-master

### Features ###

* Collaborative Commenting with up/downvote system
* Personal notes including markdown
* Related information, such as people, locations, organizations or general entities, via advanced entity linking techniques
* Related articles via information retrieval and data mining techniques

### In Action ###
![Screen Shot 2015-10-04 at 02.48.19.png](https://bitbucket.org/repo/x5nbb6/images/1137144766-Screen%20Shot%202015-10-04%20at%2002.48.19.png)
serverUrl = "http://localhost:8080/"

console.log("Test");

function login(email, user_name, password){
    $.ajax({
        type: 'post',
        url: serverUrl + 'api/users/login',
        data: JSON.stringify({
            "email": email,
            "user_name": user_name,
            "password": password
        }),
        contentType: "application/json",
        dataType: "json",
        async: false
    }).done(function (data) {
        console.log("1..");
        console.log(data);
        email = data.email;
        userId = data._id;

        chrome.cookies.set({
            url: serverUrl,
            name: "convene-login-userid",
            value: "" + userId
        }, function (cookie) {
            console.log("2..");
            chrome.cookies.set({
                url: serverUrl,
                name: "convene-login-email",
                value: email
            }, function (cookie) {
                console.log("sending logged in msg..");

                chrome.runtime.sendMessage({message: "convene logged in"}, function(response) {
                    console.log(response.message);
                    chrome.tabs.getCurrent(function(tab) {
                        chrome.tabs.remove(tab.id, function() { });
                        console.log("Logged in, set cookies!");
                    });
                });

                return false;
            });
        });
    }).fail(function(xhr, error){
        // TODO: handle errors by asking user to re-login
        console.debug(xhr);
        console.debug(error);
        return false;
    });
    console.debug("wtf");
}

function register(email, user_name, full_name, password){
    $.ajax({
        type: 'post',
        url: serverUrl + 'api/users/',
        data: JSON.stringify({
            "email": email,
            "user_name": user_name,
            "full_name": full_name,
            "password": password
        }),
        contentType: "application/json",
        dataType: "json",
        async: false
    }).fail(function(xhr, error){
        // TODO: handle errors by asking user to re-login
        console.debug(xhr);
        console.debug(error);
    });
}
$("#login").submit(function(event) {
    var clickedButton = $("input[type=submit][clicked=true]").val();

    console.log("Submit clicked!");
    email = $("input[name=email]").val();
    full_name = $("input[name=full_name]").val();
    user_name = $("input[name=user_name]").val();
    password = $("input[name=password]").val();
    if(clickedButton == "Register"){
        register(email, user_name, full_name, password);
    } else {
        login(email, user_name, password);
    }

    return false;
});

$("form input[type=submit]").click(function() {
    $("input[type=submit]", $(this).parents("form")).removeAttr("clicked");
    $(this).attr("clicked", "true");
});
debugger;
initialized = {};
overlayed = {};
var user = {loggedIn: false, email: null, id: null};

serverUrl = "http://localhost:8080/"

// When the action icon is clicked, we need to show or hide the overlay
chrome.browserAction.onClicked.addListener(function(tab) {
    if(!user.loggedIn){
        chrome.tabs.create({url: "login/login.html"})
    }else{
        chrome.tabs.query({
            active: true,
            currentWindow: true
        }, function(tabs) {
            tabId = tabs[0].id;
            if (!initialized[tabId]) {
                // This is the first time the icon was clicked for the current tab, initialize content script
                initializeOverlay(tabId);
            } else {
                // Content script is running, we just need to tell it to show or hide the overlay
                toggleOverlayVisibility(tabId);
            }
        });
    }

});

chrome.runtime.onMessage.addListener(
    function(request, sender, sendResponse) {
        console.log(sender.tab ? "from a content script:" + sender.tab.url : "from the extension");
        console.log(request.message);
        if (request.message == "escape"){
            tabId = sender.tab.id;
            toggleOverlayVisibility(tabId);
            sendResponse({message: "see you later!"});
        } else if (request.message == "convene logged in") {
            readLoggedInUser(function () {
                sendResponse({message: "awesome"});
            });
        } else if (request.message == "get user"){
            sendResponse({message: user.id});
        } else{
            //todo: error
            console.log("WTF?");
        }
        return true;
    }
);

// when the URL changes or the page is refreshed, both initialized and overlayed need to change to false for that tab
chrome.webNavigation.onCommitted.addListener(function(details) {
    if (details.frameId == 0) { // only reset if the nav is tab-level
        resetTabOverlayState(details.tabId);
    }
});

function initializeOverlay(tabId) {
    console.log('Adding first overlay to page!');
    chrome.tabs.insertCSS(tabId, { file: "style/font-awesome.css"}, function() {
        chrome.tabs.insertCSS(tabId, { file: "style/jquery.upvote.css"}, function() {
            chrome.tabs.insertCSS(tabId, { file: "style/foundation.css" }, function() {
                chrome.tabs.insertCSS(tabId, { file: "style/style.css" }, function() {
                    executeScripts(tabId, [
                            { file: "jquery/jquery-2.1.4.min.js" },
                            { file: "jquery.upvote.js" },
                            { file: "foundation.js" },
                            { file: "markdown.js" },
                            { file: "overlay_logic/content_script.js"}
                        ], function() {
                            openOverlay(tabId);
                            console.log("Overlay loaded and opened.");
                            initialized[tabId] = true;
                            overlayed[tabId] = true;
                    });
                });
            });
        });
    });
}

function resetTabOverlayState(tabId) {
    console.log("Setting tab " + tabId + " to uninitialized.")
    initialized[tabId] = false;
    overlayed[tabId] = false;
}

function toggleOverlayVisibility(tabId) {
    if (overlayed[tabId]) {
        closeOverlay(tabId);
    } else {
        openOverlay(tabId);
    }
}

function openOverlay(tabId) {
    chrome.tabs.query({'active': true, 'lastFocusedWindow': true}, function (tabs) {
        var url = tabs[0].url;
        sendMessageToTab(tabId, "open overlay|" + url)
        overlayed[tabId] = true;
    });
}

function closeOverlay(tabId) {
    sendMessageToTab(tabId, "close overlay")
    overlayed[tabId] = false;
}

function sendMessageToTab(tabId, message_) {
    console.log("Sending message '" + message_ + "' to tab " + tabId);
    chrome.tabs.sendMessage(
        tabId, {
            message: message_
        },
        function(response) {
            if(response !== undefined) {
                console.log("Response: '" + response.message + "'");
            }
        }
    );
}

function readLoggedInUser(callback) {
    chrome.cookies.get({
        url: serverUrl, name: "convene-login-userid"
    }, function (cookie) {
        user.id = cookie.value;
        chrome.cookies.get({
            url: serverUrl, name: "convene-login-email"
        }, function (cookie) {
            user.loggedIn = true;
            user.email = cookie.value;
            console.log("User logged in!");
            callback();
        });
    });
}

function executeScripts(tabId, injectDetailsArray, callback) {
    function createCallback(tabId, injectDetails, innerCallback) {
        return function() {
            chrome.tabs.executeScript(tabId, injectDetails, innerCallback);
        };
    }
    for (var i = injectDetailsArray.length - 1; i >= 0; --i) {
        callback = createCallback(tabId, injectDetailsArray[i], callback);
    }
    if (callback !== null) {
        callback(); // execute outermost function
    }
}

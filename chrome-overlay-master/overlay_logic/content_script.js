$(document).ready(function() {

console.log("Content script executing.");
//serverUrl = "46.101.226.48:49000";
//analysisUrl = "46.101.226.48:8080";
serverUrl = "http://localhost:8080/"

function getSignedInUser(callback){
    chrome.runtime.sendMessage({message: "get user"}, function(response) {
        userId = response.message;
        callback(userId);
    });
}

function fetchComments() {
    $('#comments_load').html('Comments are loading... <i class="fa fa-2x fa-circle-o-notch fa-spin"></i>');
    $.getJSON(serverUrl + "api/comments", {"url" : encodeURIComponent(document.URL) }, function(result) {
        if(result.comments.length == 0) {
            $('#comments_load').text("No comments yet. Post something!");
        } else {
            $.each(result.comments, function(index, comment) {
                createComment(comment);
            });

            $('#comments_load').detach();
        }

    }).fail(function(xhr, textStatus, errorThrown) {
        console.log(textStatus);
        $('#comments_load').text("Failed to load comments. Retry?").click(function() {
            fetchComments();
        });
    });

}

function fetchContextInfo(){
  $.getJSON(serverUrl + "api/analyze/", { "url": encodeURIComponent(document.URL) } , function(result) {
      populateCards(result.cards);
      populateArticles(result.related_articles);
    }).fail(function() {
        $("#card_loading").text("Failed to load cards and articles. Retry?").click(function () {
            fetchContextInfo();
        });
    });
}

function populateCards(cards) {
    $.each(cards, function(index, card) {
        thing = ('<div class="card row">' +
                '   <div class="row">' +
                '       <div class="card-head large-12 columns">' +
                '           <img align="left" src="#img#" class="small">' +
                '           <div class="card-title">#title#</div>' +
                '           <div class="card-category">#category#</div>' +
                '       </div>' +
                '   </div>' +
                '   <div class="cards-expander hide">' +
                '       <div class="cards-abstract">' +
                '           #abstract#' +
                '       </div>' +
                '       <div class="cards-facts hidden row">' +
                '           #facts#' +
                '       </div>' +
                '       <div class="cards-link"><a target="_blank" href="#link#">Read more</a></div>' +
                '   </div>' +
                '   ' +
                '</div>').replace("#img#", card.image == null ? chrome.extension.getURL('images/noimage.jpg') : card.image).replace("#title#", card.title)
                         .replace("#abstract#", card.abstract).replace("#link#", card.link).replace('#category#', card.category);
        fact = '           <div class="cards-fact large-12 columns">' +
                '               <span class="cards-fact-key">#key#:</span> ' +
                '               <span class="cards-fact-value">#value#</span>' +
                '           </div>';
        facts = '';
        if(card.content == null || card.content.length == 0) {
            // nothing is fine
        } else {
            $.each(card.content, function(key, value) {
                var omnom = '';
                if(key == 'Google maps') {
                    var vstring = '<a target="_blank" href="' + value + '">open in maps</a>';
                    omnom = fact.replace('#key#', key).replace('#value#', vstring) + "\n";
                } else {
                    omnom = fact.replace('#key#', key).replace('#value#', value) + "\n";
                }
                facts += omnom;
            });
        }
        thing = thing.replace('#facts#', facts);
        elem = $(thing);
        $("#card_host").append(elem);
        createCard(elem)
    });
    $("#card_loading").hide();
}

function populateArticles(articles) {
    $.each(articles, function(index, article) {
        thing = ('<div class="card row">' +
                '   <div class="row">' +
                '       <div class="card-title card-head large-12 columns">' +
                '           <img align="left" src="#img#" class="small">' +
                '           #title#' +
                '       </div>' +
                '   </div>' +
                '   <div class="cards-expander hide">' +
                '       <div class="cards-abstract">' +
                '           #text#' +
                '       </div>' +
                '       <div class="article_published">' +
                '           #published#' +
                '       </div>' +
                '       <div class="cards-link"><a target="_blank" href="#link#">Read more</a></div>' +
                '   </div>' +
                '   ' +
                '</div>').replace("#img#", article.image == null ? chrome.extension.getURL('images/noimage.jpg') : article.image).replace("#title#", article.title)
                         .replace("#text#", article.text).replace("#link#", article.url).replace("#published#", article.timestamp);

        elem = $(thing);
        $("#articles_host").append(elem);
        createCard(elem)
    });
    $("#article_loading").hide();
}


function createComment(data) {
    var ups = Math.random() * 50;
    var downs = Math.random() * 20;
    var score = parseInt(ups - downs);

    var commentitem =  ('<div data-id="#commentid#" data-state="neutral" data-basescore="#bscr#" data-score="#scr#" class="large-12 columns commentroot">' +
                    '<div class="row comment">' +
                    '    <div class="large-1 columns profilepic">' +
                    '        <img src="http://www.alangoo.com/images/profilepics/thumbImage/profile.png">' +
                    '    </div>' +
                    '    <div class="large-10 columns comment_text">' +
                    '        <div class="comment_username">#username#</div>' +
                    '        <div class="comment_text_content">#content#</div>' +
                    '    </div>' +
                    '    <div class="upvote large-1 columns">' +
                    '        <a class="upvote">' +
                    '            <i class="fa fa-chevron-up"></i>' +
                    '        </a>' +
                    '        <span class="count">0</span>' +
                    '        <a class="downvote">' +
                    '            <i class="fa fa-chevron-down"></i>' +
                    '        </a>' +
                    '    </div>' +
                    '</div>' +
                    '</div>').replace("#commentid#", data._id).replace("#content#", data.content).replace("#username#", "Anonymous")
                             .replace('#bscr#', score).replace('#scr#', score);
    var comment = $(commentitem);
    $('#comment_host').append(comment);

    // init voting
    comment.find('a.upvote').click(function() {
        console.log('clicked')
        setVoting($(comment), 'up');
    });
    comment.find('a.downvote').click(function() {
        setVoting($(comment), 'down');
    });

    setVoting($(comment), 'neutral');
    // remove no comments string
    $('#comments_load').detach();
}

function setVoting(commentx, clickedOn) {
    var state = commentx.attr('data-state');
    var score = commentx.attr('data-basescore');

    var newstate = '';

    if(clickedOn == state) {
        newstate = 'neutral';
    } else {
        newstate = clickedOn;
    }

    var num = parseInt(score);
    var displayscore = newstate == 'up' ? num + 1 : newstate == 'down' ? num - 1 : num;
    commentx.find('span').text(displayscore);
    commentx.attr('data-state', newstate);
    commentx.attr('data-score', displayscore);
    if(newstate == 'up') {
        commentx.find('a.upvote').addClass('chosen');
        commentx.find('a.downvote').removeClass('chosen');
    } else if(newstate == 'down') {
        commentx.find('a.upvote').removeClass('chosen');
        commentx.find('a.downvote').addClass('chosen');
    } else {
        commentx.find('a.upvote').removeClass('chosen');
        commentx.find('a.downvote').removeClass('chosen');
    }

    reorderComments();
}

function reorderComments() {
    var $wrapper = $('#comment_host');
    $wrapper.find('.commentroot').sort(function (a, b) {
        return +b.dataset.score - +a.dataset.score;
    })
    .appendTo( $wrapper );
}

// todo integrate me
function postcomment(content) {
    // todo add loading thingy to comment submit form
    $('#comment_submit').html('Sending comment... <i class="fa fa-lg fa-circle-o-notch fa-spin"></i>');
    $.ajax({
        url: serverUrl + "api/comment",
        type: 'post',
        dataType: 'json',
        contentType: "application/json",
        processData: false,
        data: JSON.stringify({ "url": document.URL, "content": $("#comment_text").val() })
    }).done(function(response) {
        // todo add comment to frontend now with correct id
        data = response.comment;
        data.upvotes = 0;
        data.downvotes = 0;
        createComment(data);
        $('#comment_submit').text('Post');
        $('#comment_text').val('');
    }).fail(function() {
        $('#comment_submit').text('Posting failed. Retry?');
    });
}

function createCard(data) {
    $(data).click(function() {
        $(this).children(".cards-expander").toggleClass("hide");
        var issmall = $(this).find('img').hasClass("small");
        $(this).find("img").toggleClass("small");
        // if(issmall) {
        //     $(this).find("img").unwrap();
        // } else {
        //     $(this).find("img").wrap('<span class="smallimagewrapper"></span>');
        // }
    });
}

function fetchNote() {
    $('#notes_load').html('Notes are loading... <i class="fa fa-2x fa-circle-o-notch fa-spin"></i>');
    getSignedInUser(function(userId){
        $.getJSON(serverUrl + "api/notes/" + userId + "/" + encodeURIComponent(document.URL), function(result) {
            console.log(result.note);
            if(result.note == null) {
                $('#notes_load').text("");
            } else {
                $('#note_text').val(result.note.content);
                $('#notes_load').detach();
                $('#note_submit').click();
            }
        }).fail(function(xhr, textStatus, errorThrown) {
            console.log(textStatus);
            $('#comments_load').text("Failed to load comments. Retry?").click(function() {
                fetchComments();
            });
        });
    });
}

function putNote() {
    $('#note_submit').html('Posting note... <i class="fa fa-lg fa-circle-o-notch fa-spin"></i>')
    getSignedInUser(function (userId) {
        $.ajax({
            url: serverUrl + "api/notes/" + userId + "/" + encodeURIComponent(document.URL),
            type: 'post',
            dataType: 'json',
            contentType: "application/json",
            processData: false,
            data: JSON.stringify({"text": $("#note_text").val() })
        }).done(function(response) {
            $('#note_submit').text('Success!');
            setTimeout(function() {
                $('#note_submit').text('Post');
            }, 3000);
        }).fail(function() {
            $('#note_submit').text('Posting failed. Retry?').click(function() {
                putNote();
            });
        });
    });
}


// function displayOverlay() {
//   console.log("Adding overlay to page.")
//   $("body").wrap(wrapper);
//   $("#wrapper").prepend(overlayDiv);
// };

// function removeOverlay(){
//   console.log("Removing overlay!");
//   $("#overlay").detach();
//   $("body").unwrap();
// };

$(document).foundation(); // init foundation
console.log("Adding overlay to page.")
var xurl = chrome.extension.getURL("overlay_logic/overlay.html");
overlayDiv = 'nothing';
$.get(xurl, function(result){
    overlayDiv = result;
    $("html").prepend($(overlayDiv));
    setTimeout(function() {
        $('#gotocomments').click(function() {
            console.log('world.explode');
            $('#panel2').fadeOut(function() {
                $('#panel1').fadeIn();
            });
            $('#gotocomments').parent().addClass('active');
            $('#gotonotes').parent().removeClass('active');
        });
        $('#gotonotes').click(function() {
            console.log('world.explode');
            $('#panel1').fadeOut(function() {
                $('#panel2').fadeIn();
            });
            $('#gotonotes').parent().addClass('active');
            $('#gotocomments').parent().removeClass('active');
        });
        $('#comment_submit').click(function() {
            console.log("send clicked");
            postcomment($('#comment_text').text());
        });

        $("#note_submit").click(function(){
            var host = $("#note_host");
            var text = $("#note_text").val();
            var html = markdown.toHTML(text);
            host.html(html);
            putNote();
            // TODO: submit to server, and retrieve on startup
        });
        fetchComments();
        fetchNote();
        fetchContextInfo();
    }, 100);


});

$(document).keyup(function(e) {
     if (e.keyCode == 27) {
        chrome.runtime.sendMessage({message: "escape"}, function(response) {
            console.log(response.message);
        });
     }
 });

 

function displayOverlay() {
  $("#overlay").hide().fadeIn("slow");
  $("#overlayBlend").hide().fadeIn("slow");
};

function removeOverlay(){
  console.log("Removing overlay!");
  $("#overlay").fadeOut("slow");
  $("#overlayBlend").fadeOut("slow");
};

chrome.runtime.onMessage.addListener(
    function(request, sender, sendResponse) {
        console.log(sender.tab ? "from a content script:" + sender.tab.url : "from the extension");
        if (request.message == "close overlay"){
            removeOverlay();
            sendResponse({message: "goodbye!"});
        } else if (request.message.startsWith("open overlay")) {
            displayOverlay();
            sendResponse({message: "hello!"});
        }
    }
);

});

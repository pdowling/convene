__author__ = 'dowling'
import requests
from urllib import quote_plus
import json

#url = "https://www.washingtonpost.com/news/post-politics/wp/2015/10/02/when-asked-about-school-shootings-jeb-bush-says-stuff-happens/"
urls = [
    "http://in.reuters.com/article/2015/10/03/usa-shooting-oregon-gunstore-idINKCN0RX04120151003",
    "http://www.nbcnews.com/storyline/oregon-college-shooting/oregon-college-shooting-nine-victims-rampage-are-identified-n437881",
    "https://www.whitehouse.gov/the-press-office/2015/10/01/statement-president-shootings-umpqua-community-college-roseburg-oregon",
    "https://en.wikipedia.org/wiki/Umpqua_Community_College_shooting"
]
for url in urls:
    quoted = quote_plus(url)
    req_url = "http://localhost:8080/analyze?url=%s" % quoted
    #req_url = "test"
    print "requesting %s" % req_url
    res = requests.get(req_url)
    print json.dumps(json.loads(res.content), indent=4)
    print "##################################################"
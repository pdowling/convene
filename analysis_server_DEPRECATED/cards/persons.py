__author__ = 'dowling'
import logging
ln = logging.getLogger(__name__)
import json
import requests


def get_content(entity_json):
    known_for = get_known_for(entity_json)  # Known for: known_for
    years_active = get_years_active(entity_json)  # Years active: years_active
    place_label, place = get_place(entity_json)  # place_label: place
    birth_death = get_birth_and_death(entity_json)  # Lived: birth_death

    content = {}
    if known_for:
        content["Known for"] = known_for
    if years_active:
        content["Years active"] = years_active
    if place_label and place:
        content[place_label] = place
    if birth_death:
        content["Birth/death"] = birth_death

    return content


def get_known_for(entity_json):
    known_for = entity_json.get("http://dbpedia.org/property/knownFor",
                                entity_json.get("http://dbpedia.org/ontology/office",
                                                entity_json.get("http://dbpedia.org/property/shortDescription", [])))
    if known_for:
        known_for = known_for[0]["value"]

    return known_for


def get_years_active(entity_json):
    final = None
    years_active_start = entity_json.get("http://dbpedia.org/ontology/activeYearsStartDate", [])
    if years_active_start:
        years_active_start = years_active_start[0]["value"]
        final = str(years_active_start)

    years_active_end = entity_json.get("http://dbpedia.org/ontology/activeYearsEndDate", [])
    if years_active_end:
        years_active_end = years_active_end[0]["value"]
        if final:
            final += " - " + str(years_active_end)
        else:
            final = "until " + years_active_end
    else:
        if final:
            final += "until today"

    return final


def get_place(entity_json):
    """
    try:
        http://dbpedia.org/property/residence -> get name
        http://dbpedia.org/property/deathPlace
        http://dbpedia.org/property/placeOfBirth
        http://dbpedia.org/property/birthPlace

    """
    place_label = "Residence"
    place = entity_json.get("http://dbpedia.org/property/residence", [])
    if place:
        obj = place[0]
        if obj["type"] == "uri":
            try:
                from cards import extract_title
                place_json = json.loads(requests.get(obj["value"].replace("/resource/", "/data/") + ".json").content)
                place = extract_title(place_json, None)
            except:
                ln.exception("Couldn't load uri / parse json for place %s" % obj)
                place = None
        else:
            place = obj["value"]

    if place:
        return place_label, place

    place_label = "Place of death"
    place = entity_json.get("http://dbpedia.org/property/deathPlace", [])
    if place:
        place = place[0]["value"]
        return place_label, place

    place_label = "Place of birth"
    place = entity_json.get("http://dbpedia.org/property/placeOfBirth", [])
    if place:
        place = place[0]["value"]
        return place_label, place

    place = entity_json.get("http://dbpedia.org/property/birthPlace", [])
    if place:
        place = place[0]["value"]
        return place_label, place

    return None, None


def get_birth_and_death(entity_json):
    final = None
    born = entity_json.get("http://dbpedia.org/ontology/birthDate",
                           entity_json.get("http://dbpedia.org/property/birthDate", []))
    if born:
        born = born[0]["value"]
        final = str(born)

    died = entity_json.get("http://dbpedia.org/ontology/deathDate",
                           entity_json.get("http://dbpedia.org/property/deathDate", []))
    if died:
        died = died[0]["value"]
        if final:
            final += " - " + str(died)
        else:
            final = "until " + str(died)
    else:
        if final:
            final += " - today"

    return final


__author__ = 'dowling'
import logging
ln = logging.getLogger(__name__)
import spotlight
import requests
import json
import persons
import orgs
import locations
from math import log
from collections import defaultdict

from spotlight import SpotlightException

from redis_cache import cache_it

MAX_CARDS = 10
MAX_LEN_ABSTRACT = 300

# card categories
PERSON = "Person"
LOCATION = "Location"
ORGANIZATION = "Organization"
CONCEPT = "Other concept"


def get_cards(text=None, url=None):
    if not text:
        # TODO find some other cards to create from the url?
        return []
    try:
        annotations = spotlight.annotate(
            "http://philippd.me:8080/rest/annotate",
            text,
            confidence=0.45,
            support=30,
            spotter='Default'
        )
    except SpotlightException:
        ln.warn("No entities at all for text: '%s'" % text)
        annotations = []

    entity_frequencies = defaultdict(int)
    for annotation in annotations:
        entity_frequencies[annotation["URI"]] += 1

    for annotation in annotations:
        annotation["relevancy"] = compute_tf_idf_score(annotation, entity_frequencies)

    #annotations.sort(key=lambda annotation: -annotation["support"])
    annotations.sort(key=lambda annotation: -annotation["relevancy"])

    uniques = []
    seen = set()
    for annotation in annotations:
        if annotation["URI"] not in seen:
            uniques.append(annotation)
        seen.add(annotation["URI"])

    cards = [create_card(annotation) for annotation in uniques[:MAX_CARDS]]

    return cards


def compute_tf_idf_score(annotation, entity_frequencies):
    tf = 1.0 + log(int(entity_frequencies[annotation["URI"]]))
    idf = log(4500000 / annotation["support"])  # roughly the number of entities in dbpedia / articles in wikipedia
    return tf * idf


#@cache_it(limit=60 * 60)
def create_card(annotation):
    """
    Sample annotation:
    {u'similarityScore': 0.9997483538244266,
     u'surfaceForm': u'Angela Merkel',
     u'support': 713,
     u'offset': 0,
     u'URI': u'http://dbpedia.org/resource/Angela_Merkel',
     u'percentageOfSecondRank': 0.0,
     u'types': u'DBpedia:Agent,Schema:Person,Http://xmlns.com/foaf/0.1/Person,DBpedia:Person,DBpedia:OfficeHolder'}
    Sample card:
    {
        "title": "Angela Merkel",
        "abstract": ""
    }
    We handle Persons, Cities, Countries, Companies, Organizations separately
        -> always show wikipedia link
    """
    uri = annotation["URI"]
    category = detect_category(annotation)

    json_link = uri.replace("/resource/", "/data/") + ".json"

    res = requests.get(json_link)
    res_json = json.loads(res.content)
    entity_json = res_json[uri]

    abstract = extract_abstract(entity_json)
    title = extract_title(entity_json, annotation["surfaceForm"])

    main_link, other_links = extract_links(entity_json)

    image = extract_image(entity_json)

    if not image:
        ln.warn("No image found for entity %s!" % uri)

    card = {
        "title": title,
        "abstract": abstract or "No abstract found.",
        "image": image,
        "link": main_link,
        "other_links": other_links,
        "category": category,
        "relevancy": annotation["relevancy"]
    }

    content = []
    if category == PERSON:
        content = persons.get_content(entity_json)
    elif category == LOCATION:
        content = locations.get_content(entity_json)
    elif category == ORGANIZATION:
        content = orgs.get_content(entity_json)
    else:
        pass

    card["content"] = content

    ln.debug("Found information on entity %s: %s" % (title, card))
    return card


def detect_category(annotation):
    types = annotation["types"]

    final_type = None
    if "DBpedia:Person" in types:
        final_type = PERSON
    elif "DBpedia:Place" in types:
        final_type = LOCATION
    elif "DBpedia:Organisation" in types:
        final_type = ORGANIZATION

    if final_type is None:
        ln.warn("No type found for entity %s. Types were %s" % (annotation["surfaceForm"], types))
        final_type = CONCEPT

    return final_type


def extract_title(entity_json, default):
    ln.debug("Got %s as default." % default)
    title = entity_json.get("http://dbpedia.org/property/name",
                            entity_json.get("http://xmlns.com/foaf/0.1/name", []))
    if title:
        title = title[0]["value"]
    else:
        title_cands = entity_json.get("http://www.w3.org/2000/01/rdf-schema#label", [])
        for title_cand in title_cands:
            if title_cand["lang"] == "en":
                ln.debug("Chose best title")
                title = title_cand["value"]

        if not title:
            if len(title_cands):
                ln.debug("Chose first title")
                title = title_cands[0]["value"]
            else:
                ln.warn("No title for entity, choosing default (%s)." % default)
                ln.debug("Chose default title")
                title = default
    ln.debug("Returning name %s" % title)
    return title


def extract_abstract(entity_json):
    abstracts = entity_json.get("http://dbpedia.org/ontology/abstract", [])
    final_abstract = None

    for lang in ["de", "en"]:
        for abstract in abstracts:
            if abstract["lang"] == lang:
                final_abstract = abstract["value"]

    if final_abstract is None:
        if len(abstracts):
            final_abstract = abstracts[0]
        else:
            ln.warn("No abstract for entity %s" % entity_json)
            final_abstract = ""

    if len(final_abstract) > MAX_LEN_ABSTRACT:
        final_abstract = final_abstract[:MAX_LEN_ABSTRACT] + "..."

    return final_abstract


def extract_links(entity_json):
    main_link = entity_json.get("http://xmlns.com/foaf/0.1/isPrimaryTopicOf", [])
    if main_link:
        main_link = main_link[0]["value"]
    other_links = entity_json.get("http://dbpedia.org/ontology/wikiPageExternalLink", [])
    other_links = [link["value"] for link in other_links]
    if not main_link and other_links:
        main_link = other_links[0]
    return main_link, other_links


def extract_image(entity_json):
    image = entity_json.get("http://xmlns.com/foaf/0.1/depiction", [])
    if image:
        image = image[0]["value"]
    ln.debug(image)
    return image or None
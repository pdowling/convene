__author__ = 'dowling'
import logging
logging.basicConfig(format="%(asctime)s %(levelname)-8s %(name)-18s: %(message)s", level=logging.DEBUG)
ln = logging.getLogger(__name__)

from flask import Flask, request
from urllib import unquote

import requests
import json

from extract import extract_text_and_images
from cards.cards import get_cards
from related import get_related_pages


from redis_cache import SimpleCache
from redis_cache import cache_it


from werkzeug.routing import PathConverter


class EverythingConverter(PathConverter):
    regex = '.*?'


app = Flask(__name__)
app.url_map.converters['everything'] = EverythingConverter
app.debug = True
# DATA FLOW
#  - download HTML
#  - extract plain text
#  - call spotlight and build cards
#  - query for related articles
#  OPTIONAL:
#  - search for images
#  - search for files to integrate in dropbox


@app.route("/analyze")
def analyze_url():
    url = request.args.get("url")
    if url is None:
        return 300
    url = unquote(url)
    return analyze_content(url)


@cache_it(expire=60 * 5)
def analyze_content(url):
    print "Analyzing page at '%s'" % url
    response = requests.get(url)
    content_html = response.text
    raw_text, top_image, goose_obj = extract_text_and_images(content_html, return_goose=True)
    # TODO use top image
    if raw_text != "":
        entity_cards = get_cards(text=raw_text)
        related_articles = get_related_pages(url, goose_obj=goose_obj)
    else:
        entity_cards = get_cards(url=url)
        related_articles = get_related_pages(url=raw_text)

    return json.dumps({
        "cards": entity_cards,
        "related_articles": related_articles
    })


app.run("0.0.0.0", 8080)
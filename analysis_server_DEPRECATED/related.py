__author__ = 'dowling'
import logging
ln = logging.getLogger(__name__)
from elasticsearch import Elasticsearch
import datetime
import json

# proposed solution:
# throw document into ES
# have crawler also throw docs into ES
# query ES using "more like this" and return short dict of related articles

es = Elasticsearch(hosts=[{'host': '46.101.226.48', 'port': 9200}])
#http://46.101.226.48:9200
def get_related_pages(url, goose_obj=None):
    if not goose_obj:
        return []  # TODO

    try:
        image = goose_obj.top_image.src
    except AttributeError:
        ln.debug("No image for %s" % url)
        image = None

    page_doc = {
        "title": goose_obj.title,
        "text": goose_obj.cleaned_text,
        "image": image,
        "url": url,
        "timestamp": datetime.datetime.now()
    }
    res = es.index(index="articles", doc_type="page", body=page_doc, id=hash(url))

    mlt_es = es.mlt("articles", "page", res["_id"], mlt_fields=["title", "text"])
    ln.debug(json.dumps(mlt_es, indent=4))
    if mlt_es["hits"]["total"] != 0:
        mlt_hits = mlt_es["hits"]["hits"]
        mlt = [hit["_source"] for hit in mlt_hits]
        for related in mlt:
            if len(related["text"]) > 300:
                related["text"] = related["text"][:300] + "..."
            related["timestamp"] = related["timestamp"][:related["timestamp"].find(".")]
        return mlt
    else:
        return []


"""
curl -XGET '46.101.181.125:9200/twitter/_search?search_type=scan&scroll=10m&size=50' -d '
{
    "query" : {
        "match_all" : {}
    }
}'
curl -XPUT 'http://46.101.181.125:9200/twitter/tweet/1' -d '{
    "user" : "kimchy",
    "post_date" : "2009-11-15T14:12:12",
    "message" : "trying out Elasticsearch"
}'

curl -XGET 'http://46.101.181.125:9200/twitter/mlt' -d '{
  "query" : {
    "more_like_this" : {
      "like_text" : "trying",
      "min_term_freq" : 1,
      "min_doc_freq" : 1
    }
  }
}'

curl -XGET 'http://46.101.181.125:9200/twitter/tweet/1/_mlt?mlt_fields=tag,content&min_doc_freq=1'

curl -XDELETE 'http://46.101.181.125:9200:9200/articles/'
"""
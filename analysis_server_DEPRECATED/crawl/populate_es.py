__author__ = 'dowling'
import logging
logging.basicConfig(format="%(asctime)s %(levelname)-8s %(name)-18s: %(message)s", level=logging.DEBUG)
ln = logging.getLogger(__name__)
from elasticsearch import Elasticsearch
import feedparser
from goose import Goose
import datetime
import time
import requests

def get_urls():
    with open("crawl_urls.txt") as f:
        return map(lambda x: x.strip(), f.readlines())

es = Elasticsearch(hosts=[{'host': '46.101.226.48', 'port': 9200}])
#http://:9200
UPDATE_INTERVAL = 60 * 5  # seconds

etags = {}
modified = {}

g = Goose()

iknow = set()


def update_all_feeds():
    ln.debug("Updating all feeds...")
    for url in get_urls():
        res = feedparser.parse(url, etag=etags.get(url, None), modified=modified.get(url, None))

        try:
            etags[url] = res.etag
        except AttributeError:
            ln.warn("No etag for url %s" % url)
            try:
                modified[url] = res.modified
            except AttributeError:
                ln.warn("No modified either for url %s" % url)
                pass

        if res.status != 304:
            # we have an update
            ln.info("Got %s entries from feed %s" % (len(res.entries), url))
            for entry in res.entries:
                if entry.link not in iknow:
                    try:
                        full = requests.get(entry.link).content
                        goosed = g.extract(raw_html=full)
                        text = goosed.cleaned_text
                    except:
                        ln.exception("Something went wrong parsing url %s. Ignoring in future." % url)
                        iknow.add(entry.link)
                        continue

                    try:
                        image = goosed.top_image.src
                        ln.debug("Found image for %s" % url)
                    except AttributeError:
                        ln.debug("No image for %s" % url)
                        image = None

                    if text != "":
                        page_doc = {
                            "title": entry.title,
                            "text": text,
                            "image": image,
                            "url": entry.link,
                            "timestamp": datetime.datetime.now()  # yolo
                        }
                        yield page_doc
                    iknow.add(entry.link)


def go():
    running = True
    while running:
        started = time.time()
        for new_item in update_all_feeds():
            r = es.index("articles", "page", new_item, id=hash(new_item["url"]))

        time.sleep(max(UPDATE_INTERVAL - (time.time() - started), 10))
__author__ = 'dowling'
import logging
ln = logging.getLogger(__name__)

import requests
import json


def get_content(entity_json):
    lat, lon = get_lat_lon(entity_json)
    type = get_location_type(entity_json)
    if lat and lon:
        maps_link = get_maps_link(lat, lon, type)
    else:
        maps_link = None
    pop = get_population(entity_json)
    leadertitle, leadername = get_leader(entity_json)

    content = {
        "Type": type,
        "Google maps": maps_link,
        "Population": pop,
        leadertitle: leadername
    }

    return content


def get_lat_lon(entity_json):
    lat = entity_json.get("http://www.w3.org/2003/01/geo/wgs84_pos#lat",
                          entity_json.get("http://dbpedia.org/property/latitude",
                                          entity_json.get("http://dbpedia.org/property/lats", [])))
    if lat:
        lat = lat[0]["value"]

    lon = entity_json.get("http://www.w3.org/2003/01/geo/wgs84_pos#long",
                          entity_json.get("http://dbpedia.org/property/longitude",
                                          entity_json.get("http://dbpedia.org/property/longs", [])))
    if lon:
        lon = lon[0]["value"]

    return lat or None, lon or None


def get_location_type(entity_json):
    labels = ["Location", "Country", "State", "City"]
    precision = {
        "http://dbpedia.org/ontology/Place": 0,
        "http://umbel.org/umbel/rc/Country": 1,
        "http://schema.org/Country": 1,
        "http://dbpedia.org/ontology/Country": 1,
        "http://dbpedia.org/ontology/PopulatedPlace": 1,
        "http://dbpedia.org/ontology/AdministrativeRegion": 2,
        "http://umbel.org/umbel/rc/Location_Underspecified": 2,
        "http://dbpedia.org/ontology/Region": 2,
        "http://umbel.org/umbel/rc/City": 3,
        "http://dbpedia.org/ontology/Settlement": 3
    }
    types = entity_json.get("http://www.w3.org/1999/02/22-rdf-syntax-ns#type")
    best = 0
    for type in types:
        label = type["value"]
        if label in precision:
            precision_val = precision[label]
            if precision_val > best:
                best = precision_val

    return labels[best]


def get_maps_link(lat, lon, type):
    z = {
        "Location": 5,
        "Country": 6,
        "State": 8,
        "City": 10
    }
    return "https://www.google.com/maps/@%s,%s,%sz" % (lat, lon, z[type])


def get_population(entity_json):
    pop = entity_json.get("http://dbpedia.org/ontology/populationTotal",
                          entity_json.get("http://dbpedia.org/property/populationTotal", []))
    if pop:
        pop = pop[0]["value"]
    return pop or None


def get_leader(entity_json):
    leadername = entity_json.get("http://dbpedia.org/property/leaderName",
                                 entity_json.get("http://dbpedia.org/ontology/leaderName", []))
    if leadername:
        if leadername[0]["type"] == "uri":
            from cards import extract_title
            try:
                new_uri = leadername[0]["value"].replace("/resource/", "/data/") + ".json"
                leadername_json = json.loads(requests.get(new_uri).content)[leadername[0]["value"]]
                leadername = extract_title(leadername_json, leadername[0]["value"])
            except:
                ln.exception("Error looking up title for %s" % leadername[0]["value"])
                leadername = leadername[0]["value"]
        else:
            leadername = leadername[0]["value"]

    leadertitle = entity_json.get("http://dbpedia.org/ontology/leaderTitle",
                                  entity_json.get("http://dbpedia.org/property/leaderTitle", []))
    if leadertitle:
        if leadertitle[0]["type"] == "uri":
            from cards import extract_title
            try:
                new_uri = leadertitle[0]["value"].replace("/resource/", "/data/") + ".json"
                leadertitle_json = json.loads(requests.get(new_uri).content)[leadertitle[0]["value"]]
                leadertitle = extract_title(leadertitle_json, leadertitle[0]["value"])
            except:
                ln.exception("Error looking up title for %s" % leadertitle[0]["value"])
                leadertitle = leadertitle[0]["value"]
        else:
            leadertitle = leadertitle[0]["value"]


    return leadertitle or "", leadername or None
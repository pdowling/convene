import requests
from extract import extract_text_and_images
from related import get_related_pages
from cards.cards import get_cards
from redis_cache import cache_it


@cache_it(expire=60 * 5)
def analyze_content(url):
    print "Analyzing page at '%s'" % url
    response = requests.get(url)
    content_html = response.text
    raw_text, top_image, goose_obj = extract_text_and_images(content_html, return_goose=True)

    # TODO use top image
    entity_cards = get_cards(text=raw_text)
    related_articles = get_related_pages(url, goose_obj=goose_obj)

    return {
        "cards": entity_cards,
        "related_articles": related_articles
    }
__author__ = 'dowling'
import logging
ln = logging.getLogger(__name__)
from goose import Goose

g = Goose()


def extract_text_and_images(html, return_goose=False):
    try:
        article = g.extract(raw_html=html)
    except:
        ln.exception("Error extracting html.")
        if return_goose:
            return "", None, None
        else:
            return "", None

    src = article.top_image.src if article.top_image else None
    if not return_goose:
        return article.cleaned_text, src
    else:
        return article.cleaned_text, src, article


__author__ = 'dowling'
from start_server import app as application
from werkzeug.wsgi import DispatcherMiddleware
from werkzeug.serving import run_simple
from flask import Flask, send_from_directory

static_app = Flask(__name__ + "_static")


@static_app.route("/", defaults={'path': 'index.html'})
@static_app.route('/<path:path>')
def server_static(path):
    return send_from_directory('../frontend/', path)

app = DispatcherMiddleware(
    static_app,
    {
        "/api": application,
    }
)


if __name__ == "__main__":
    run_simple('0.0.0.0', 8080, app)

__author__ = 'dowling'

"""
Start the server. Some code taken from the eleza.org codebase (but since I'm a dev there, I'm allowing it).

"""
import logging

FORMAT = "%(asctime)s %(levelname)-8s %(name)-18s: %(message)s"
fmt = logging.Formatter(FORMAT)
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
ch.setFormatter(fmt)
logging.basicConfig(
    filename="tracedeck-log.out",
    format=FORMAT,
    level=logging.DEBUG
)
logging.getLogger("").addHandler(ch)
ln = logging.getLogger(__name__)

from flask import Flask, send_from_directory
from flask_login import LoginManager

from backend.util import status
from backend.model.user import get_user_by_id

from werkzeug.wsgi import DispatcherMiddleware
from werkzeug.serving import run_simple
from werkzeug.routing import PathConverter
import os


class ConfigClass(object):
    SECRET_KEY = os.getenv('TRACEDECK_SECRET_KEY',
                           'tracedeck+apoasdv$"Tsdfi?c_&r7-as3edd=ol$v-.gh3_x)ydakhbsakjhgblol?')
    CSRF_ENABLED = True
    APPLICATION_ROOT = "/api"

    USE_EMAIL_AUTH = False  # TODO once mail is set up, change this

    MAIL_SERVER = 'todo'  # TODO set up mail
    MAIL_PORT = 587  # TODO set up mail
    # MAIL_PORT = 143

    # MAIL_USE_SSL = True
    MAIL_USE_TLS = True  # TODO set up mail

    MAIL_USERNAME = 'm03707d5'
    MAIL_PASSWORD = os.getenv('TRACEDECK_MAIL_PASSWORD', '')  # TODO set up mail
    MAIL_DEFAULT_SENDER = ('Tracedeck', 'hello@tracedeck.co')

    HOST_NAME = os.getenv('HOST_NAME', '')

    UPLOAD_FOLDER = "uploads/"

    SQLALCHEMY_DATABASE_URI = 'sqlite:///tracedeck.sqlite'


def create_app():
    app = Flask(__name__)
    app.config.from_object(__name__ + '.ConfigClass')

    from backend.model.db import db
    db.app = app
    db.init_app(app)
    db.create_all()

    from backend.tasks.redis_store import redis_store
    redis_store.init_app(app)

    @app.after_request
    def add_header(response):
        response.headers['Access-Control-Allow-Origin'] = '*'
        response.headers['Access-Control-Allow-Headers'] = 'Origin, X-Requested-With, Content-Type, Accept'
        return response

    login_manager = LoginManager()
    login_manager.init_app(app)

    @login_manager.unauthorized_handler
    def unauthorized_handler():
        return status.dump_json(status.Status.UNAUTHORIZED)

    @login_manager.user_loader
    def load_user(user_id):
        user_obj = get_user_by_id(user_id)
        if user_obj:
            app.logger.debug('Getting user with id %s, name %s, authenticated %d, role %s'
                             % (user_id, user_obj.name, user_obj.is_authenticated, user_obj.role))
        return user_obj

    from backend.util.mail_service import mail
    mail.app = app
    mail.init_app(app)

    from backend.endpoints.users import users_blueprint
    from backend.endpoints.analyze import analyze_blueprint
    from backend.endpoints.notes import notes_blueprint
    app.register_blueprint(users_blueprint)
    app.register_blueprint(analyze_blueprint)
    app.register_blueprint(notes_blueprint)

    logging.getLogger("elasticsearch").setLevel(logging.DEBUG)

    return app


class EverythingConverter(PathConverter):
    regex = '.*?'


app = create_app()
app.url_map.converters['everything'] = EverythingConverter
app.debug = True

if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description='Process some stuff')
    parser.add_argument('port', type=int, help='port number', default=8080)
    args = parser.parse_args()
    port = args.port

    static_app = Flask(__name__ + "_static")


    @static_app.route("/", defaults={'path': 'index.html'})
    @static_app.route('/<path:path>')
    def server_static(path):
        return send_from_directory('frontend/app/', path)


    app = DispatcherMiddleware(
        static_app,
        {
            "/api": app,
        }
    )

    run_simple('0.0.0.0', port, app)

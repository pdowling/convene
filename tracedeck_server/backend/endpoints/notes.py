__author__ = 'dowling'
import logging
from flask import request, Blueprint
from flask_login import login_required, current_user
from urllib import unquote
from backend.model.note import create_or_update_note, get_note
from backend.util import status

from backend.util.json_tools import dump_json, load_json

logging.basicConfig(format="%(asctime)s %(levelname)-8s %(name)-18s: %(message)s", level=logging.DEBUG)
ln = logging.getLogger(__name__)

notes_blueprint = Blueprint('notes', __name__, url_prefix='/notes')


@notes_blueprint.route("/<user_id>/<path:url>", methods=["POST"])
@login_required
def post_note(user_id, url):
    if str(user_id) != str(current_user._id):
        return status.dump_json(status.Status.FORBIDDEN)

    if url is None:
        return 300
    url = unquote(url)

    data = load_json(request.data)
    note = create_or_update_note(current_user._id, url, text=data["text"])
    return dump_json(note.as_dict())


@notes_blueprint.route("/<user_id>/<path:url>", methods=["GET"])
@login_required
def get_note_for_page(user_id, url):
    if str(user_id) != str(current_user._id):  # TODO could enable note sharing
        return status.dump_json(status.Status.FORBIDDEN)

    note = get_note(current_user._id, url)

    return dump_json(note.as_dict())
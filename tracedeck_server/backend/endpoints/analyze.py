from backend.celery.analyze import async_analyze_content, get_task

__author__ = 'dowling'
import logging
from flask import request, Blueprint
from flask_login import login_required
from urllib import unquote
from backend.util.json_tools import dump_json, load_json
from backend.util import status
from flask_login import current_user

logging.basicConfig(format="%(asctime)s %(levelname)-8s %(name)-18s: %(message)s", level=logging.DEBUG)
ln = logging.getLogger(__name__)

analyze_blueprint = Blueprint('analyze', __name__, url_prefix='/analyze')


@analyze_blueprint.route("/", methods=['POST'])
@login_required
def analyze_url():
    data = load_json(request.data)
    url = data.get("url", None)
    if url is None:
        return 300

    url = unquote(url)
    task = async_analyze_content(url, str(current_user._id))
    return dump_json({"task_id": task.id})


@analyze_blueprint.route("/<task_id>", methods=['GET'])
@login_required
def get_analysis_results(task_id):
    task = get_task(task_id)

    if task is None:
        return status.dump_json(status.Status.NOT_FOUND, message="No such task.")

    if task.headers["user_id"] != str(current_user._id):
        return status.dump_json(status.Status.FORBIDDEN, message="You don't have permission to view this task.")

    if task.state == 'PENDING':
        response = {
            'state': task.state,
            'current': 0,
            'total': 1,
            'status': 'Pending...'
        }
    elif task.state != 'FAILURE':
        response = {
            'state': task.state,
            'current': task.info.get('current', 0),
            'total': task.info.get('total', 1),
            'status': task.info.get('status', '')
        }
        if 'result' in task.info:
            response['result'] = task.info['result']
    else:
        # something went wrong in the background job
        response = {
            'state': task.state,
            'current': 1,
            'total': 1,
            'status': str(task.info),  # this is the exception raised
        }
    return dump_json(response)

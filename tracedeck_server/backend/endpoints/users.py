__author__ = 'dowling'
import logging
ln = logging.getLogger(__name__)
from flask import Blueprint, request, jsonify
from flask import request

from flask_login import login_user, login_required, logout_user, current_user
from backend.model.user import auth_user
from backend.util.verify import admin_only


from backend.util.json_tools import load_json, dump_json, dump_user, prep_user
from backend.util import status
from backend.model.user import ROLE_ADMIN, ROLE_USER, create_user, get_user_by_id, get_user_by_email, get_user_by_activation_code

users_blueprint = Blueprint('users', __name__, url_prefix='/users')


@users_blueprint.route("/<user_id>", methods=["GET"])
@login_required
def get_user(user_id):
    if current_user.id != user_id and current_user.role != ROLE_ADMIN:
        return status.dump_json(status.Status.FORBIDDEN)

    user = get_user_by_id(user_id)

    return dump_user(user.as_dict())


@users_blueprint.route("/", methods=["POST"])
def post_user():
    user_data = load_json(request.data)

    existing_user = get_user_by_email(email=user_data["email"].lower())
    if existing_user is not None:
        return status.dump_json(status.Status.FORBIDDEN)

    role = user_data.get("role", ROLE_USER)
    if role not in [ROLE_ADMIN, ROLE_USER]:
        return status.dump_json(status.Status.BAD_REQUEST)

    if role == ROLE_ADMIN and getattr(current_user, "role", None) != ROLE_ADMIN:
        ln.warn("Someone tried to register an operator account without being logged in or an operator themselves.")
        return status.dump_json(status.Status.FORBIDDEN)

    created_user = create_user(
        username=user_data["user_name"],
        full_name=user_data["full_name"],
        email=str(user_data["email"].lower()),
        password=user_data["password"],
        role=role
    )

    return dump_user(created_user.as_dict())


@users_blueprint.route('/login', methods=['POST'])
def login():
    user_data = load_json(request.data)

    email = user_data['email'].lower()
    password = user_data['password']

    user = auth_user(email, password)

    if user is None:
        return status.dump_json(status.Status.EMAIL_OR_PW_INVALID)
    else:
        if login_user(user, remember=True):
            user.log_in()
            return dump_user(user.as_dict())
        else:
            return status.dump_json(status.Status.EMAIL_OR_PW_INVALID)


@users_blueprint.route('/check_login', methods=['GET', 'POST'])
@login_required
def check_login():
    return dump_user(current_user)


@users_blueprint.route('/logout', methods=['GET'])
@login_required
def logout():
    current_user.log_out()
    logout_user()
    return status.dump_json(status.Status.OK)


@users_blueprint.route('/activate')
def activate():
    code = request.args.get('code')

    user = get_user_by_activation_code(code)
    if user is not None:
        user.activate()
        return status.dump_json(status.Status.OK)
    else:
        return status.dump_json(status.Status.BAD_REQUEST, message="Code not valid for any user.")

__author__ = 'dowling'
from flask import current_app
from backend.model.db import Base, db
from sqlalchemy import Column, Integer, String, Boolean, Unicode, DateTime, Sequence
# from flask.ext.mongokit import Document
from passlib.hash import pbkdf2_sha512
import binascii
import random
import datetime
import os
from sqlalchemy.orm.exc import NoResultFound

import logging
ln = logging.getLogger(__name__)

ROLE_USER = u"ROLE_USER"
ROLE_ADMIN = u"ROLE_ADMIN"  # TODO refactor to admin

ROLES = [ROLE_USER, ROLE_ADMIN]


class Note(Base):
    __tablename__ = 'notes'

    user_id = Column(Integer, primary_key=True)
    created = Column(DateTime)
    text = Column(Unicode)
    url = Column(String, primary_key=True)


def get_note(user_id, url):
    return Note.query.filter_by(user_id=user_id, url=url).first()


def create_or_update_note(user_id, url, text):
    note = get_note(user_id, url)
    if note is None:
        note = Note(
            user_id=user_id,
            url=url,
            text=text,
            created=datetime.datetime.now()
        )
    else:
        note.text = text

    db.session.add(note)
    db.session.commit()

    return note

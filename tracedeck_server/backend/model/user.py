__author__ = 'dowling'
from flask import current_app
from backend.model.db import Base, db
from sqlalchemy import Column, Integer, String, Boolean, Unicode, DateTime, Sequence
# from flask.ext.mongokit import Document
from passlib.hash import pbkdf2_sha512
import binascii
import random
import datetime
import os
from sqlalchemy.orm.exc import NoResultFound

import logging
ln = logging.getLogger(__name__)

ROLE_USER = u"ROLE_USER"
ROLE_ADMIN = u"ROLE_ADMIN"  # TODO refactor to admin

ROLES = [ROLE_USER, ROLE_ADMIN]


class User(Base):
    __tablename__ = 'users'

    _id = Column(Integer, Sequence('user_id_seq'), primary_key=True)
    name = Column(String, unique=True)
    fullname = Column(Unicode)
    email = Column(String, unique=True)
    password_hash = Column(String)
    is_authenticated = Column(Boolean)
    created = Column(DateTime)
    is_active = Column(Boolean)
    activation_code = Column(String, unique=True)
    role = Column(String)

    def get_id(self):
        return self._id

    def log_in(self):
        self.is_authenticated = True
        db.session.add(self)
        db.session.commit()

    def log_out(self):
        self.is_authenticated = False
        db.session.add(self)
        db.session.commit()

    def activate(self):
        self.activation_code = ''
        self.is_active = True
        db.session.add(self)
        db.session.commit()


def create_user(username, full_name, email, password, role, _override_active_to_true=False):
    assert role in ROLES
    password_hash = pbkdf2_sha512.encrypt(password, rounds=100000, salt_size=16)
    is_active = (not current_app.config["USE_EMAIL_AUTH"]) or _override_active_to_true

    activation_code = binascii.b2a_hex(os.urandom(15))
    while User.query.filter_by(activation_code=activation_code).first():
        activation_code = binascii.b2a_hex(os.urandom(15))

    user = User(
        name=username,
        fullname=full_name,
        email=email.lower(),
        password_hash=password_hash,
        is_authenticated=False,
        created=datetime.datetime.now(),
        is_active=is_active,
        activation_code=activation_code,
        role=role
    )

    if (not _override_active_to_true) and current_app.config["USE_EMAIL_AUTH"]:
        from backend.util.mail_service import send_registration_confirmation
        send_registration_confirmation(user)

    db.session.add(user)
    db.session.commit()

    return user


def get_user_by_id(user_id):
    try:
        return User.query.filter_by(_id=user_id).first()
    except NoResultFound:
        return None


def get_user_by_email(email):
    try:
        return User.query.filter_by(email=email).first()
    except NoResultFound:
        return None


def get_user_by_activation_code(code):
    try:
        return User.query.filter_by(activation_code=code).first()
    except NoResultFound:
        return None


def simulate_auth():
    pbkdf2_sha512.verify(
        "".join([random.choice("abcdefghijklmnopqrstuvwxyz") for _ in range(24)]),
        pbkdf2_sha512.encrypt(
            "".join([random.choice("abcdefghijklmnopqrstuvwxyz") for _ in range(20)]), rounds=100000, salt_size=16
        )
    )


def auth_user(email, password):
    try:
        user = get_user_by_email(email)
    except NoResultFound:
        simulate_auth()
        ln.debug("No user found for email %s" % email)
        user = None

    if user is not None:
        if pbkdf2_sha512.verify(password, user.password_hash):
            return user
        else:
            return None
    else:
        simulate_auth()
        return None

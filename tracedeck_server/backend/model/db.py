__author__ = 'dowling'
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


def as_dict(self):
    return {c.name: getattr(self, c.name) for c in self.__table__.columns}

db.Model.as_dict = as_dict

Base = db.Model

import backend.model.note
import backend.model.user



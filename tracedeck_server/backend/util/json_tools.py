from bson import json_util  # json.dump(json_, f, default=json_util.default, indent=4)
import json


def dump_json(bson):
    return json.dumps(bson, default=json_util.default, indent=4)


def prep_user(user_dict):
    user = {
        key: user_dict[key] for key in ["name", "email", "created", "is_authenticated", "is_active", "role", "_id"]
    }
    return user


def dump_user(user_dict):
    return json.dumps(prep_user(user_dict), default=json_util.default, indent=4)


def load_json(json_):
    return json.loads(json_, object_hook=json_util.object_hook)


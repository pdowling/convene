import logging
ln = logging.getLogger(__name__)
from flask import current_app
from flask_mail import Mail, Message
import datetime
import pickle
mail = Mail()


def send_registration_confirmation(user):
    message = Message('Registration at Simon Does', recipients=[user.email])

    host = current_app.config["HOST_NAME"]

    message.body = 'Dear %s,\n\nThanks for registering with tracedeck.io.\n\nClick the following link to activate your \
                    account: %s/api/users/activate?code=%s \n' % (user.user_name, host, user.activation_code)

    mail.send(message)


def send_email_message(user, subject, message_text):
    message = Message(subject, recipients=[user.email])

    # TODO add some html to the message
    message.body = message_text

    mail.send(message)

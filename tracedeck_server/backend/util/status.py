from flask import jsonify
from collections import namedtuple


class Status_(namedtuple('Status', 'code message')):
    def to_dict(self):
        return self._asdict()


class Status(object):
    OK = Status_(200, 'ok')
    UNAUTHORIZED = Status_(401, 'Unauthorized')
    FORBIDDEN = Status_(403, "Forbidden")
    NOT_FOUND = Status_(404, 'Not Found')
    INTERNAL_SERVER_ERROR = Status_(500, 'Internal Server Error')
    BAD_REQUEST = Status_(400, 'Bad Request')
    EMAIL_OR_PW_INVALID = Status_(401, 'Email or password invalid')


def dump_json(status, **kwargs):
    dictionary = {'status': status.to_dict()}

    for key, value in kwargs.items():
        dictionary[key] = value

    response = jsonify(dictionary)
    response.status_code = status.code

    return response
sudo apt-get update
sudo apt-get -y upgrade
sudo apt-get install -y python-pip
sudo apt-get install -y git
sudo apt-get install -y gunicorn
# INSTALL MONGODB
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv EA312927
echo "deb http://repo.mongodb.org/apt/ubuntu trusty/mongodb-org/3.2 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.2.list
sudo apt-get update
sudo apt-get install -y mongodb-org

# SET UP TRACEDECK
git clone https://pdowling@bitbucket.org/pdowling/convene.git
cd convene/flask_server
sudo pip install -r requirements.txt
chmod +x wsgi_start.sh
chmod +x start_mongodb.sh



### Starting up

## Start up MongoDB (it might already be running)
# export LC_ALL=C
# ./start_mongodb.sh

## Start the mail client (this is important, otherwise user mails won't occur in the simon backend)
## In a new window:
# ./start_email_checker.sh

## OPTIONAL: Set up DB with test users
# python manager_script.py setup_database

## Start the actual server on port 8080
# ./wsgi_start.sh